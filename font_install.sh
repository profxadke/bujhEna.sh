#!/usr/bin/bash

if [ -n "$1" ]; then
	echo "$0 [PATH.to.FONT.zip]"
fi

if [ $UID != 0 ]; then
	echo "[-] UserSpace install not available here."
	exit 1
fi

RDIR=".$RANDOM.$RANDOM.$RANDOM.$RANDOM"
TDIR="/tmp/.fIns/$RDIR"

mkdir -p "$TDIR" && cd $TDIR
echo -n "Enter font name to be used as dir. ? "
read -r fdname
mkdir $fdname && cd $fdname && unzip $1 && cd ->/dev/null
mv $fdname "/usr/local/share/fonts/$fdname"
echo "[~] Wait, Caching fonts .."
fc-cache
rm -rf "$TDIR"

echo "[+] $1 installed asin within/like $fdname DIR ?"
